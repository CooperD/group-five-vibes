var DailyColor = require('../models/dailyColor');
var mongoose = require('mongoose');
var User = require('../models/user');

//Get colors for homepage.
exports.getColors = async function () {
  let d = new Date();
  d.setDate(d.getDate() - 30);
  let colors = await DailyColor.find({ 'date': { $gte: d } });
  let pastMonth = [];

  for (let i = 0; i < colors.length; i++) {
    //Find which color. 
    //This defaults to whichever is listed first in case of a tie.
    if (colors[i].num_yellow >= colors[i].num_red
      && colors[i].num_yellow >= colors[i].num_blue
      && colors[i].num_yellow >= colors[i].num_green
      && colors[i].num_yellow >= colors[i].num_purple) {
      pastMonth.push({ date: colors[i].date, color: 'yellow' })
    }
    else if (colors[i].num_red >= colors[i].num_yellow
      && colors[i].num_red >= colors[i].num_blue
      && colors[i].num_red >= colors[i].num_green
      && colors[i].num_red >= colors[i].num_purple) {
      pastMonth.push({ date: colors[i].date, color: 'red' })
    }
    else if (colors[i].num_blue >= colors[i].num_yellow
      && colors[i].num_blue >= colors[i].num_red
      && colors[i].num_blue >= colors[i].num_green
      && colors[i].num_blue >= colors[i].num_purple) {
      pastMonth.push({ date: colors[i].date, color: 'blue' })
    }
    else if (colors[i].num_green >= colors[i].num_yellow
      && colors[i].num_green >= colors[i].num_blue
      && colors[i].num_green >= colors[i].num_red
      && colors[i].num_green >= colors[i].num_purple) {
      pastMonth.push({ date: colors[i].date, color: 'green' })
    }
    else if (colors[i].num_purple >= colors[i].num_yellow
      && colors[i].num_purple >= colors[i].num_blue
      && colors[i].num_purple >= colors[i].num_green
      && colors[i].num_purple >= colors[i].num_red) {
      pastMonth.push({ date: colors[i].date, color: 'purple' })
    }
  }
  return pastMonth;
}

exports.DailyColor_Create = async function () {
  let numRed = 0, numBlue = 0, numGreen = 0, numPurple = 0, numYellow = 0;
  let d = new Date();
  console.log("cron job started");


  
  for await (const user of User.find()) {
    switch (userColor) {
      case 'red':
        numRed++;
        break;
      case 'blue':
        numBlue++;
        break;
      case 'green':
        numGreen++;
        break;
      case 'yellow':
        numYellow++;
        break;
      case 'purple':
        numPurple++;
        break;
    }
  }
    
  const dailycolor = new DailyColor(
    {
      date: d.getTime(),
      num_yellow: numYellow,
      num_red: numRed,
      num_blue: numBlue,
      num_green: numGreen,
      num_purple: numPurple
    }
  )

  dailycolor.save(function (err) {
    if (err) {
      console.log(JSON.stringify(err));
      return;
    }
  });
}