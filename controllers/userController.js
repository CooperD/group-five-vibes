var mongoose = require('mongoose');
var User = require('../models/user');
var crypto = require('crypto');
var SecurityQ = require('../models/securityQ');
var { body, validationResult } = require('express-validator');



const hashPassword = (pwd) => {
  const sha256 = crypto.createHash('sha256');
  const hash = sha256.update(pwd).digest('base64');

  return hash;
}

//add user
exports.userinstance_create_post = [

  body('SecurityAnswer', 'Empty SecurityAnswer')
    .trim()
    .isLength({ min: 1 })
    .escape(),

  body('firstName', 'Invalid First Name')
    .trim()
    .isLength({ min: 1 })
    .escape(),

  body('lastName', 'Invalid Last Name')
    .trim()
    .isLength({ min: 1 })
    .escape(),

  body('email')
    .trim()
    .isLength({ min: 1 }).withMessage("Please enter valid email")
    .normalizeEmail()
    .isEmail().withMessage('Please enter valid email'),

  body('password')
    .isLength({ min: 8 }).withMessage('Password must be 8 characters long')
    .matches('[0-9]').withMessage('Password must include number')
    .matches('[a-z]|[A-Z]').withMessage('Password must include letter'),

  body('username')
    .custom(value => {
      return User.findOne({ 'user_name': value }).then(user => {
        if (user) {
          return Promise.reject('Username already in use');
        }
      });
    }),
  (req, res, next) => {
    var errors = validationResult(req);

    if (!errors.isEmpty()) {
      req.session.error = errors.array()[0].msg;
      console.error('ERROR: ' + req.session.error);
      res.redirect('/register');
      return;
    }

    var securityQuestion = new SecurityQ(
      {
        question: req.body.SecurityQuestion,
        answer: req.body.SecurityAnswer
      }
    );
    securityQuestion.save(function (err) {
      if (err) { return next(err); }
    })

    const hashedpwd = hashPassword(req.body.password);

    let user = new User(
      {
        first_name: req.body.firstName,
        last_name: req.body.lastName,
        user_name: req.body.username,
        email: req.body.email,
        current_color: " ",
        password: hashedpwd,
        security_question: securityQuestion._id
      }
    );
    user.save(function (err) {
      if (err) {
        req.session.error = JSON.stringify(err);
        res.redirect('/register');
        return;
      }
      res.redirect('/signin');
      return;
    });

  }
];

//log in user
exports.user_log_in = function (req, res, next) {

  uname = req.body.username;
  const hashedpwd = hashPassword(req.body.password);

  User.findOne({ 'user_name': req.body.username, 'password': hashedpwd }, 
                function (err, login) {
    if (err || login === null) {
      req.session.error = 'Incorrect username or password'
      res.redirect('/signin');
    }
    else {
      req.session.user_id = login._id;
      res.redirect('/home');
    }
  });
}

//update user info
exports.update_user = function (req, res, next) {
  User.findOne({ '_id': req.session.user_id }, function (err, thisuser) {
    if (req.body.firstName) {
      thisuser.first_name = req.body.firstName;
    }
    if (req.body.lastName) {
      thisuser.last_name = req.body.lastName;
    }
    if (req.body.email) {
      thisuser.email = req.body.email;
    }
    if (req.body.username) {
      thisuser.user_name = req.body.username;
    }
    if (req.body.password && req.body.vPassword && 
        req.body.password === req.body.vPassword) {
      thisuser.password = hashPassword(req.body.password);
    }
    thisuser.save();
    res.redirect('/home');
  });
}

// user color
exports.user_color = function (req, res, next) {
  User.findOne({ '_id': req.session.user_id }, function (err, thisuser) {
    console.log('Retrieve/update the users color choice');
    console.log(JSON.stringify(req.body.colorSelection));
    if (req.body.colorSelection) {
      thisuser.current_color = req.body.colorSelection;
    }

    thisuser.save();
    res.redirect('/home');
  });

  //return req.body.colorSelection;
}


exports.find_user = async function (id) {
  var user = await User.findOne({ '_id': id });
  return user;
}