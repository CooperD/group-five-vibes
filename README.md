# README: GroupFiveVibes

<img src="public/images/icon-opt-one.png" height=50> <img src="public/images/home-banner.png" height=50>

## Description:
This project is a website based in express that allows users to pick a color
that describes their emotional state and aggregates that so you can see peoples'
overall vibe for a given day.

## Installation:
To make a copy of this website that runs locally:

### 1. Clone this repository. 
(execute the command `git clone https://gitlab.com/CooperD/group-five-vibes.git` 
in the desired parent directory)


### 2. If you have not already, install node
([https://nodejs.org/en/](https://nodejs.org/en/))


### 3. Install dependencies
(In the created project directory, execute the command `npm install`)

### 4. Decide whether local or online mongodb.
#### 4.1. If you want to use a local mongodb
a. If you do not already have mongodb installed, install it.
(Follow the directions here: 
[https://docs.mongodb.com/manual/administration/install-community/](https://docs.mongodb.com/manual/administration/install-community/))

b. Populate your local database.
(In the project directory, execute the command 
`node populatedb.js mongodb://localhost:27017/GroupFiveVibes`)

#### 4.2. If you want to use an online mongodb
a. Open app.js. Uncomment in line 26 and comment out line 25. 
(The two lines starting with "var mongodb")

### 5. Run node
(In the project directory, execute the command `npm run start`)


### 6. Visit the page 
[http://localhost:3000](http://localhost:3000) in your browser to view the local server.
