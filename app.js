var createError = require('http-errors');
var express = require('express');
var cron = require('node-cron');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');


var dailyColor_controller = require('./controllers/dailyColorController');

//Parser for logins
var bodyParser = require('body-parser');

var app = express();

app.locals.moment = require('moment');

//db setup
var mongoose = require('mongoose');
//local mongodb url
var mongoDB = 'mongodb://localhost:27017/GroupFiveVibes?retryWrites=true&w=majority';
//remote mongodb url
//var mongoDB = 'mongodb+srv://backend-user:J6KbOsDE26QzDTso@slaanesh.pz2wm.mongodb.net/GroupFiveVibes?retryWrites=true&w=majority';
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


//cron job to collect colors
cron.schedule('0 0 * * *', function () {
  dailyColor_controller.DailyColor_Create();
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ secret: 'this is required if this were real we would want it random but security is low priority right now' }));
app.use(function (req, res, next) {
  res.locals.session = req.session;
  next();
})

//Use the parser
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

//express.use('public');
