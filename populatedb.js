#! /usr/bin/env node

console.log('This script populates the last 30 days with daily colors');

// Get arguments passed on command line
let userArgs = process.argv.slice(2);

let DailyColorSchema = require('./models/dailyColor');
let d = new Date();
let millisecondsInADay = 8.64e+7;

d.getDate();

let mongoose = require('mongoose');
let mongoDB = userArgs[0];

mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


function colorCreate(date, num_yellow, num_red, num_blue, num_green, num_purple) {
    colordetail = {
        date: date, num_yellow: num_yellow, num_red: num_red,
        num_blue: num_blue, num_green: num_green, num_purple: num_purple
    }

    return color = new DailyColorSchema(colordetail);
}

function createColors() {
    let i;
    for (i = 0; i <= 30; ++i) {
        let color = colorCreate(d - i * millisecondsInADay, Math.floor(Math.random() * 100),
            Math.floor(Math.random() * 100), Math.floor(Math.random() * 100),
            Math.floor(Math.random() * 100), Math.floor(Math.random() * 100));
        console.log('New Daily Color: ' + color);
        color.save();
    }
}

createColors();
setTimeout(() => { mongoose.connection.close(); }, 3000);