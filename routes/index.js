var express = require('express');
const app = require('../app');
var router = express.Router();
var color_controller = require('../controllers/dailyColorController');
var user_controller = require('../controllers/userController');



/* GET home page. */
router.get('/', function (req, res, next) {
  res.redirect('/home');
});

router.get('/home', async function (req, res, next) {
  var notifs = req.session.notification;
  var errors = req.session.error;
  let currentUser = await user_controller.find_user(req.session.user_id);
  let userColor = ((currentUser) ? currentUser.current_color : " ");

  console.log(userColor);

  delete req.session.error;
  delete req.session.notification;

  color_controller.getColors().then(monthColors => {
    res.render('index', { title: 'ColorVIBES', error: errors, notification: notifs, colors: monthColors, currentColor: userColor });
  }, reason => {
    res.render('index', { title: 'ColorVIBES', error: errors, notification: notifs });
    console.log(reason);
  })
});

// GET signin page.
router.get('/signin', function (req, res, next) {
  var notifs = req.session.notification;
  var errors = req.session.error;

  delete req.session.error;
  delete req.session.notification;
  res.render('signin', { title: 'Sign in', error: errors });

});

// GET register page.
router.get('/register', function (req, res, next) {
  var notifs = req.session.notification;
  var errors = req.session.error;

  delete req.session.error;
  delete req.session.notification;
  res.render('register', { title: 'Register', error: errors });
});

// GET account page.
router.get('/accounts', function (req, res, next) {
  var notifs = req.session.notification;
  var errors = req.session.error;

  delete req.session.error;
  delete req.session.notification;
  res.render('accounts', { title: 'Account', errors });
});

module.exports = router;
