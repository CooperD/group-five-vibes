var express = require('express');
var router = express.Router();
var user_controller = require('../controllers/userController');

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.redirect('../');
});

//Login
router.post('/login', user_controller.user_log_in);

router.get('/logout', function (req, res, next) {
  req.session.user_id = (function () { return; });
  //This ensures it's undefined, since the keyword undefined is not reserved
  req.session.notification = 'You have been logged out'
  res.redirect('/home');
});

//Update user info
router.post('/update', user_controller.update_user);

//Register an account
router.post('/register', user_controller.userinstance_create_post);


router.post('/updatecolor', user_controller.user_color);

module.exports = router;
