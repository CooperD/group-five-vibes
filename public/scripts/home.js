/**************************************************************************
 File name:     home.js
 Author:        Group Five
 Date:          11.30.2020
 Class:         CS360 Web Tech and Frameworks
 Assignment:    Final Group Project
 Purpose:       Adds functionality to the home page.
 *************************************************************************/

displayCurrentColor();
applyButtonToggle();
displayMonthColors();

/*************************************************************************
Function:     displayCurrentColor

Description:  Displays the current color choice of the user by 
              highlighting the corresponding color button.

Parameters:   None

Returned:     None
*************************************************************************/

function displayCurrentColor() {
  const EMPTY_STRING = " ";

  if (userColor != EMPTY_STRING) {
    let classChoice = "choice-" + userColor;
    let choice = document.getElementsByClassName(classChoice);

    choice[0].classList.toggle("active");
    choice[0].style.maxHeight = choice[0].scrollHeight + "px";

    choice[0].style.cssText = "border: 5px solid maroon;";
    choice[0].nextElementSibling.checked = true;
  }
}

/*************************************************************************
Function:     applyButtonToggle

Description:  Adds the event listener to the color buttons so that the 
              accordion opens when clicked.

Parameters:   None

Returned:     None
*************************************************************************/

function applyButtonToggle() {
  let acc = document.getElementsByClassName("accordion");
  for (let i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function () {
      let panel = this.nextElementSibling;

      if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
      } else {
        let active = document.querySelectorAll(".accordion-div .accordion.active");
        for (let j = 0; j < active.length; j++) {
          active[j].classList.remove("active");
          active[j].nextElementSibling.style.maxHeight = null;
          active[j].style.cssText = "border: none;";
        }

        this.classList.toggle("active");
        panel.style.maxHeight = panel.scrollHeight + "px";

        this.style.cssText = "border: 5px solid maroon;";
        panel.nextElementSibling.checked = true;
      }
    });
  }
}

/*************************************************************************
Function:     displayMonthColors

Description:  Displays the most chosen colors of the past 30 days.

Parameters:   None

Returned:     None
*************************************************************************/

function displayMonthColors() {
  let days = document.getElementsByClassName("day");
  for (let i = 0; i < days.length; i++) {
    let img = document.createElement('img');
    img.src = "images/" + local_data[i].color + 'blob.png';
    days[i].append(img);
  }
}