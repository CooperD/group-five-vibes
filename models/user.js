var mongoose = require('mongoose');
const securityQ = require('./securityQ').schema;

var Schema = mongoose.Schema;

var UserSchema = new Schema(
  {
    first_name: { type: String, required: true, maxlength: 100 },
    user_name: { type: String, required: true, unique: true, maxlength: 100 },
    last_name: { type: String, required: true, maxlength: 100 },
    email: { type: String, required: true, maxlength: 100 },
    password: { type: String, required: true, maxlength: 100 },
    security_question: { type: Schema.Types.ObjectId, ref: 'SecurityQ', required: true },
    current_color: { type: String } //hex
  }
)

UserSchema
  .virtual('url')
  .get(function () {
    return '/accounts/user/' + this._id;
  })

module.exports = mongoose.model('User', UserSchema);