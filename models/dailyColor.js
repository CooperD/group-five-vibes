var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var DailyColorSchema = new Schema(
  {
    date: { type: Date, required: true },
    num_yellow: { type: Number, required: true },
    num_red: { type: Number, required: true },
    num_blue: { type: Number, required: true },
    num_green: { type: Number, required: true },
    num_purple: { type: Number, required: true }, //hex value
  }
)

module.exports = mongoose.model('DailyColor', DailyColorSchema);